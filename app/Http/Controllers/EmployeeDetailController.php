<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeDetail;

class EmployeeDetailController extends Controller
{
    public function index()
    {
        $data = EmployeeDetail::orderBy('id', 'DESC')->get();
        return view('admin.employee.index',compact('data'));
    }

    public function create()
    {
        return view('admin.employee.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
        ]);

        $data = new EmployeeDetail;
       
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone_number = $request->phone_number;
        $data->dob = $request->dob;
        $data->gender = $request->gender;
        $data->join_of_year = $request->join_of_year;
        $data->university = $request->university;
        $data->pass_out_year = $request->pass_out_year;
        $data->cgpa_experience = $request->cgpa_experience;
        $data->save();
        return redirect()->route('admin.employee.index')->with('success', 'EmployeeDetail details create successfully.');
    }

    public function edit($id)
    {
        $data = EmployeeDetail::find($id);
        return view('admin.employee.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|min:11|numeric',
        ]);

        $data = EmployeeDetail::find($id);
      
        if (!empty($data)) {
            $data->name = $request->name;
            $data->email = $request->email;
            $data->phone_number = $request->phone_number;
            $data->dob = $request->dob;
            $data->gender = $request->gender;
            $data->join_of_year = $request->join_of_year;
            $data->university = $request->university;
            $data->pass_out_year = $request->pass_out_year;
            $data->cgpa_experience = $request->cgpa_experience;
            $data->save();
            return redirect()->route('admin.employee.index')->with('success', 'EmployeeDetail details update successfully.');
        } else {
            return redirect()->route('admin.employee.index')->with('error', 'Something went wrong.');
        }
    }

    public function delete($id)
    {
        $data = EmployeeDetail::find($id);
        if (!empty($data)) {
            $data->delete();
            return redirect()->route('admin.employee.index')->with('success', 'EmployeeDetail details delete successfully.');
        } else {
            return redirect()->route('admin.employee.index')->with('error', 'Something went wrong.');
        }
    }
}
?>