<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Education,EmployeeDetail};

class EducationController extends Controller
{
    public function index()
    {
        $data = Education::orderBy('id', 'DESC')->get();
        return view('admin.education.index', compact('data'));
    }

    public function create()
    {
        $data = EmployeeDetail::orderBy('id','DESC')->pluck('name','id')->all(); 
        return view('admin.education.create',compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'employee_id' => 'required',
            'company_name' => 'required',
            'year_of_experience' => 'required',
        ]);

        $data = new Education;
        $data->employee_id = $request->employee_id;
        $data->company_name = $request->company_name;
        $data->year_of_experience = $request->year_of_experience;
        $data->save();

        return redirect()->route('admin.education.index')->with('success', 'Education details create successfully.');
    }

    public function edit($id)
    {
        $employee = EmployeeDetail::orderBy('id','DESC')->pluck('name','id')->all(); 

        $data = Education::find($id);
        return view('admin.education.edit',compact('data','employee'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'employee_id' => 'required',
            'company_name' => 'required',
            'year_of_experience' => 'required',
        ]);

        $data = Education::find($id);
        if (!empty($data)) {
            $data->employee_id = $request->employee_id;
            $data->company_name = $request->company_name;
            $data->year_of_experience = $request->year_of_experience;
            $data->save();
            return redirect()->route('admin.education.index')->with('success', 'Education details update successfully.');
        } else {
            return redirect()->route('admin.education.index')->with('error', 'Something went wrong.');
        }
    }

    public function delete($id)
    {
        $data = Education::find($id);
        if (!empty($data)) {
            $data->delete();
            return redirect()->route('admin.education.index')->with('success', 'Education details delete successfully.');
        } else {
            return redirect()->route('admin.education.index')->with('error', 'Something went wrong.');
        }
    }
}
