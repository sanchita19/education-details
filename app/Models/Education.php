<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Prophecy\Doubler\Generator\Node\ReturnTypeNode;

class Education extends Model
{
    use HasFactory;

    protected $table = 'educations';
    protected $fillable = ['employee_id','cgpa_experience','company_name','year_of_experience'];


    public function employees(){
        return $this->hasOne(EmployeeDetail::class,'id','employee_id');
    }
}
