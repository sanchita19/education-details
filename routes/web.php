<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EducationController;
use App\Http\Controllers\EmployeeDetailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::prefix('education')->group(function () {

        Route::get('index',  [EducationController::class, 'index'])->name('admin.education.index');
        Route::get('create', [EducationController::class, 'create'])->name('admin.education.create');
        Route::post('store',  [EducationController::class, 'store'])->name('admin.education.store');
        Route::get('edit/{id}',   [EducationController::class, 'edit'])->name('admin.education.edit');
        Route::post('update/{id}', [EducationController::class, 'update'])->name('admin.education.update');
        Route::get('delete', [EducationController::class, 'delete'])->name('admin.education.delete');
    });

    Route::prefix('employee')->group(function () {

        Route::get('index',  [EmployeeDetailController::class, 'index'])->name('admin.employee.index');
        Route::get('create', [EmployeeDetailController::class, 'create'])->name('admin.employee.create');
        Route::post('store',  [EmployeeDetailController::class, 'store'])->name('admin.employee.store');
        Route::get('edit/{id}',   [EmployeeDetailController::class, 'edit'])->name('admin.employee.edit');
        Route::post('update/{id}', [EmployeeDetailController::class, 'update'])->name('admin.employee.update');
        Route::get('delete', [EmployeeDetailController::class, 'delete'])->name('admin.employee.delete');
    });
});
