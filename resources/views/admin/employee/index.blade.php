@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <p><a class="btn btn-primary" href="{{route('admin.employee.create')}}">Add Employee</a></p>
        <p><a class="btn btn-primary" href="{{route('admin.education.index')}}">Education Details</a></p>

    </div>
    <div class="row">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>DOB</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               
                @if(!empty($data))
                @foreach($data as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->phone_number}}</td>
                    <td>{{$row->dob}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{route('admin.employee.edit', $row->id)}}">Edit</a>
                        <a class="btn btn-danger" href="{{route('admin.employee.delete', $row->id)}}">Delete</a>
                       
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
          
        </table>
    </div>
</div>

@endsection
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link ref="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css"></link>
<script ref="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script ref="javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script ref="javascript" src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>