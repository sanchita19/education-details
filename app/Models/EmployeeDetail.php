<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDetail extends Model
{
    use HasFactory;

    protected $fillable = ['name','email','phone_nuber','dob','gender','join_of_year','university','pass_out_year','cgpa_experience'];
}

?>