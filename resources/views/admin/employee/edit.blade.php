@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <p><a class="btn btn-primary" href="{{route('admin.education.create')}}">Add Education Details</a></p>
  </div>
  <div class="row">
    <div class="col-lg-12">
      @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
    </div>
    <form action="{{route('admin.employee.update',$data->id)}}" method="POST">
      @csrf
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputEmail4">Name</label>
          <input type="text" class="form-control" name="name" value="{{old('name',$data->name)}}" id="inputEmail4" placeholder="Name">
        </div>
        <div class="form-group col-md-6">
          <label for="inputEmail4">Email</label>
          <input type="email" class="form-control" name="email" value="{{old('email',$data->email)}}" id="inputEmail4" placeholder="Email">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputPassword4">Phone Number</label>
          <input type="text" class="form-control" name="phone_number" value="{{old('phone_number', $data->phone_number)}}" id="inputPassword4" placeholder="phone nomber">
        </div>
        <div class="form-group col-md-6">
          <label for="inputPassword4">DOB</label>
          <input type="date" class="form-control" name="dob" value="{{old('dob', $data->dob)}}" id="inputPassword4" placeholder="dob">
        </div>
      </div>
      <div class="form-group">
        <label for="inputAddress">Gender</label>
        <select name="gender" class="form-control">
          <option name="gender" value="Male" @if($data->gender == 'Male') 'selected' @endif >Male</option>
          <option name="gender" value="Female" @if($data->gender == 'Female') 'selected' @endif >Female</option>
          <option name="gender" value="Other" @if($data->gender == 'Other') 'selected' @endif >Other</option>
        </select>
      </div>
      <div class="form-group">
        <label for="inputAddress2">Join Of The Year</label>
        <input type="date" class="form-control" name="join_of_year" value="{{old('join_of_year', $data->join_of_year)}}" placeholder="Join Of The Year">
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputCity">University</label>
          <input type="text" class="form-control" name="university" value="{{old('university', $data->university)}}" id="university">
        </div>
        <div class="form-group col-md-6">
          <label for="inputCity">Pass Out Year</label>
          <input type="date" class="form-control" name="pass_out_year" value="{{old('pass_out_year', $data->pass_out_year)}}" id="pass_out_year">
        </div>
      </div>
      <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">CGPA Experience</label>
        <input type="text" class="form-control" name="cgpa_experience" value="{{old('cgpa_experience',$data->cgpa_experience)}}" placeholder="cgpa_experience">
      </div>
      </div>
      <button type="submit" class="btn btn-primary">Save</button>
      <a class="btn btn-default" href="{{route('admin.employee.index')}}">Cancel</a>
    </form>
  </div>
</div>

@endsection
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">