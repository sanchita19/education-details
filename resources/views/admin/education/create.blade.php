@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
        </div>
        <form action="{{route('admin.education.store')}}" method="POST">
            @csrf
            <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputAddress">User Name</label>
                <select name="employee_id" class="form-control">
                    @foreach($data as $key=>$value)
                    <option name="employee_id" value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </div>
            </div>
            <div class="form-row">
             
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Company Name</label>
                    <input type="text" class="form-control" name="company_name" placeholder="company_name">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Year Of Experience</label>
                    <input type="text" class="form-control" name="year_of_experience" placeholder="year_of_experience">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <a class="btn btn-default" href="{{route('admin.education.index')}}">Cancel</a>
        </form>
    </div>
</div>

@endsection
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">