@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">

    <form action="{{route('admin.employee.store')}}" method="POST">
      @csrf
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputEmail4">Name</label>
          <input type="text" class="form-control" name="name" id="inputEmail4" placeholder="Name">
        </div>
        <div class="form-group col-md-6">
          <label for="inputEmail4">Email</label>
          <input type="email" class="form-control" name="email" id="inputEmail4" placeholder="Email">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputPassword4">Phone Number</label>
          <input type="text" class="form-control" name="phone_number" id="inputPassword4" placeholder="phone nomber">
        </div>
        <div class="form-group col-md-6">
          <label for="inputPassword4">DOB</label>
          <input type="date" class="form-control" name="dob" id="inputPassword4" placeholder="dob">
        </div>
      </div>
      <div class="form-group">
        <label for="inputAddress">Gender</label>
        <select name="gender" class="form-control">
          <option name="gender" value="Male">Male</option>
          <option name="gender" value="Female">Female</option>
          <option name="gender" value="Other">Other</option>
        </select>
      </div>
      <div class="form-group">
        <label for="inputAddress2">Join Of The Year</label>
        <input type="date" class="form-control" name="join_of_year" placeholder="Join Of The Year">
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputCity">University</label>
          <input type="text" class="form-control" name="university" id="university">
        </div>
        <div class="form-group col-md-6">
          <label for="inputCity">Pass Out Year</label>
          <input type="date" class="form-control" name="pass_out_year" id="pass_out_year">
        </div>
      </div>
      <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputEmail4">CGPA Experience</label>
        <input type="text" class="form-control" name="cgpa_experience" placeholder="cgpa_experience">
      </div>
      </div>
      <button type="submit" class="btn btn-primary">Save</button>
      <a class="btn btn-default" href="{{route('admin.employee.index')}}">Cancel</a>
    </form>
  </div>
</div>

@endsection
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">